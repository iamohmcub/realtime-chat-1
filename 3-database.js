const mongoose = require('mongoose');
const UserSchema = require('./4-database-user-schema');
const RoomSchema = require('./5-database-room.schema');

//#region connect
async function initDatabase(){
    await mongoose.connect("mongodb://localhost:27017/realtime-chat")
    mongoose.updateOne
}
//#endregion

//#region mongo function
async function findPassword(name){
    const findResult = UserSchema.findOne({name:name});
    return findResult
}
async function findAllRoom(){
    const findResult = await RoomSchema.find({}).exec();
    return findResult
}

async function createRoom(userName,roomName){
    const doc = new RoomSchema({name:userName,room:roomName})
    await doc.save();
}

async function createUser(userName,passwordSalt){
    const doc = new UserSchema({name:userName,password:passwordSalt})
    await doc.save();
}
//#endregion

module.exports = {
    initDatabase,
    findPassword,
    findAllRoom,
    createRoom,
    createUser
};

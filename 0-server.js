const express = require('express');
const http = require('http');

const initSocketio = require('./2-socket');
const onRESTRouting = require('./1-router');
const {initDatabase} = require('./3-database');

const app = express();
const server = http.createServer(app);
const port = 3000;

app.listen(port,() =>{
    console.log("server is running : ",port);
    onRESTRouting(app),
    initSocketio(server),
    initDatabase()
})


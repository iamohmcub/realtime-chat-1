const express = require('express');
const path = require('path');
const {findPassword, createUser, findAllRoom,createRoom} = require('./3-database');
const bcrypt = require('bcryptjs');

function onRESTRouting(app){
    app.use(express.urlencoded({extended: false}));
    app.use(express.json());
    app.use(express.static(path.join(__dirname,'frontend')));
    //#region FIRSTPAGE
    app.get("/",(req,res)=>{
        res.sendFile(path.join(__dirname, 'frontend/index.html'))
    })
    app.get("/wrongPassword",(req,res)=>{
        res.sendFile(path.join(__dirname, 'frontend/index-wrong-password.html'))
    })
    app.get("/userNotFound",(req,res)=>{
        res.sendFile(path.join(__dirname, 'frontend/index-user-not-found.html'))
    })
    //#endregion

    //#region middleware login
    app.post("/verifyLogin",async (req,res)=>{
        const user = await findPassword(req.body.name);
        if(user==null){
            console.log("this")
            res.redirect("/userNotFound");
        }else{
            const isCorrectPassword = bcrypt.compareSync(req.body.password,user.password)
            if(isCorrectPassword==true){
                res.redirect(`/select?name=${req.body.name}`) 
            }else{
                res.redirect("/wrongPassword")
            }
        }
    })

    app.post("/register",async (req,res)=>{
        encryptPassword = await bcrypt.hash(req.body.password, 10);
        createUser(req.body.name,encryptPassword);
        res.redirect(`/select?name=${req.body.name}`)
    })
    //#endregion

    //#region SELECT ROOM
    app.get("/select",(req,res)=>{
        res.sendFile(path.join(__dirname, 'frontend/select-room.html'))
    })
    
    app.post("/createRoom",(req,res)=>{
        console.log(req,req.body.room)
        createRoom(req.body.name,req.body.room);
        res.redirect(`/chat?name=${req.params.name}&room=${req.body.room}`)
    })
    
    app.get("/findAllRoom",async (req,res)=>{
        const allRoom = await findAllRoom()
        res.json(allRoom)
    })
    //#endregion

    //#region CHAT
    app.get("/chat",(req,res)=>{
        res.sendFile(path.join(__dirname, 'frontend/chat.html'))
    })
    //#endregion
}

module.exports = onRESTRouting;
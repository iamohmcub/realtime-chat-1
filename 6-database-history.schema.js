const mongoose = require('mongoose');

const historySchema = new mongoose.Schema({
    room: { type: String},
    history: { type: String },
})

module.exports = mongoose.model('historys', historySchema);
const socketio =require('socket.io')

function initSocketio(server){
    const io = socketio(server)
    io.on('connection',socket => {
        //#region new User
        socket.on("joinRoom", ({ username, room }) =>{
            const user = userJoin(socket.id,username,room);
            socket.join(user.room);

            socket.emit("message", formatMessage("Admin", "Welcome to ChatCord!"));
            
            socket.broadcast.to(user.room).emit("message",formatMessage(botName, `${user.username} has joined the chat`));

            io.to(user.room).emit("roomUsers", {room: user.room,users: getRoomUsers(user.room)});
        })
        //#endregion

        //#region Listen for chatMessage
        socket.on("chatMessage", (msg) => {
            const user = getCurrentUser(socket.id);
        
            io.to(user.room).emit("message", formatMessage(user.username, msg));
        });
        //#endregion

        //#region Leave Room
        socket.on("disconnect", () => {
            const user = userLeave(socket.id);
        
            if (user) {
              io.to(user.room).emit(
                "message",
                formatMessage(botName, `${user.username} has left the chat`)
              );
        
              // Send users and room info
              io.to(user.room).emit("roomUsers", {
                room: user.room,
                users: getRoomUsers(user.room),
              });
            }
          });
        //#endregion
    })
}

function formatMessage(username, text) {
    return {
        username,
        text,
        time: moment().format('h:mm a')
    };
}

const users = [];

//#region USER Utils
function userJoin(id, username, room) {
    const user = { id, username, room };
    users.push(user);
    return user;
}

function userLeave(id) {
    const index = users.findIndex(user => user.id === id);
    if (index != -1) {
      return users.splice(index, 1)[0];
    }
}
  
function getCurrentUser(id) {
    return users.find(user => user.id === id);
}

function getRoomUsers(room) {
    return users.filter(user => user.room === room);
}
//#endregion

module.exports = initSocketio;
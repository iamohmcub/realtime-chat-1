const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
    name: { type: String},
    room: { type: String },
})

module.exports = mongoose.model('rooms', roomSchema);

